using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;
using NLog;

namespace Y{

    public class ReceivedEventArgs : EventArgs {
        public byte[] ReceivedBytes { get; private set; }
        public NetworkStream NetworkStream { get; private set; }

        public ReceivedEventArgs(NetworkStream networkStream, byte[] receivedBytes) {
            this.NetworkStream = networkStream;
            this.ReceivedBytes = receivedBytes;
        }
    }
}
