﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Y;

namespace TcpServer {
    class Program {
        static void Main(string[] args) {

            var rqServer = new RqServer();

            while (true) {
                Thread.Sleep(10000);
            }
        }
    }


    class RqServer {
       private Server _server;

       public RqServer() {
           _server = new Server();
           _server.Start();
           _server.Received += _server_Received;
       }

       void _server_Received(object sender, ReceivedEventArgs e) {
           if (e.ReceivedBytes.Length >= 13) {
               var bs = new byte[] { 0x01, 0x72, 0x00, 0x00, 0x00, 0x17, 0x01, 0x03, 0x04, 0x3A, 0x00, 0x41, 0x2C };
               e.NetworkStream.Write(bs, 0, bs.Length);
           }
       }
    }
}
