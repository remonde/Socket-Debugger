﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SocketClient {
    public partial class frmReplyMbSettings : Form {
        public frmReplyMbSettings() {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e) {
            try {
                var bytes = HexStringConverter.Default.ConvertToBytes(this.txtReplybytes.Text);
                App.Default.ReplyManager.ReplyCollection.GetModbusReplyItem().ReplyBytesTemplate = bytes;

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            } catch (Exception ex) {
                NUnit.UiKit.UserMessage.DisplayFailure(ex.ToString());
            }
        }

        private void frmReplyMbSettings_Load(object sender, EventArgs e) {

            var mri = App.Default.ReplyManager.ReplyCollection.GetModbusReplyItem();
            var template = mri.ReplyBytesTemplate;
            if (template == null) {
                template = new byte[0];
            }

            this.txtReplybytes.Text = (string)HexStringConverter.Default.ConvertToObject(template);
        }

        private void textBox1_TextChanged(object sender, EventArgs e) {
            try {
                var bytes = HexStringConverter.Default.ConvertToBytes(this.txtReplybytes.Text);
                this.lblCount.Text = bytes.Length.ToString();
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);

                this.lblCount.Text = "!";
            }
        }
    }
}
