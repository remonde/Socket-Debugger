﻿namespace SocketClient
{
    partial class frmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.colDT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSCDir = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCComDir = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDatas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tssSocket = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssSerialPort = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssTransmitter = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssReply = new System.Windows.Forms.ToolStripStatusLabel();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSaveLog = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuOp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCreateConnection = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuLastConnection = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDisconnect = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuSend = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuClearLog = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuView = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuToolBar = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuStatusBar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuAscii = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHex = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSerialPort = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuOpenSerialPort = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCloseSerialPort = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuEnableTransmit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuSerialPortSetting = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuWrapperManager = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuReply = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuEnableReply = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuEnabledReplyModbus = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuReplySetting = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbCreateConnection = new System.Windows.Forms.ToolStripButton();
            this.tsbLastConnection = new System.Windows.Forms.ToolStripButton();
            this.tsbDisconnection = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbSave = new System.Windows.Forms.ToolStripButton();
            this.tsbHex = new System.Windows.Forms.ToolStripButton();
            this.tsbAscii = new System.Windows.Forms.ToolStripButton();
            this.tsbClear = new System.Windows.Forms.ToolStripButton();
            this.tsbReply = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.hexSend = new Be.Windows.Forms.HexBox();
            this.tsSend = new System.Windows.Forms.ToolStrip();
            this.tsbShowRow = new System.Windows.Forms.ToolStripButton();
            this.tsbShowCol = new System.Windows.Forms.ToolStripButton();
            this.tsbSp1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbClearSend = new System.Windows.Forms.ToolStripButton();
            this.tsbSendHistory = new System.Windows.Forms.ToolStripButton();
            this.tsbSend = new System.Windows.Forms.ToolStripButton();
            this.mnuReplyMbSettings = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tsSend.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDT,
            this.colS,
            this.colSCDir,
            this.colC,
            this.colCComDir,
            this.colCom,
            this.colLength,
            this.colDatas});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView1.Size = new System.Drawing.Size(751, 172);
            this.dataGridView1.TabIndex = 6;
            this.dataGridView1.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseDown);
            this.dataGridView1.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseUp);
            // 
            // colDT
            // 
            dataGridViewCellStyle1.Format = "yyyy-MM-dd hh:mm:ss.fff";
            dataGridViewCellStyle1.NullValue = null;
            this.colDT.DefaultCellStyle = dataGridViewCellStyle1;
            this.colDT.HeaderText = "时间";
            this.colDT.Name = "colDT";
            this.colDT.ReadOnly = true;
            this.colDT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colS
            // 
            this.colS.HeaderText = "S";
            this.colS.Name = "colS";
            this.colS.ReadOnly = true;
            this.colS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colS.Width = 40;
            // 
            // colSCDir
            // 
            this.colSCDir.HeaderText = "-";
            this.colSCDir.Name = "colSCDir";
            this.colSCDir.ReadOnly = true;
            this.colSCDir.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colSCDir.Width = 40;
            // 
            // colC
            // 
            this.colC.HeaderText = "C";
            this.colC.Name = "colC";
            this.colC.ReadOnly = true;
            this.colC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colC.Width = 40;
            // 
            // colCComDir
            // 
            this.colCComDir.HeaderText = "-";
            this.colCComDir.Name = "colCComDir";
            this.colCComDir.ReadOnly = true;
            this.colCComDir.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colCComDir.Width = 40;
            // 
            // colCom
            // 
            this.colCom.HeaderText = "Com";
            this.colCom.Name = "colCom";
            this.colCom.ReadOnly = true;
            this.colCom.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colCom.Width = 40;
            // 
            // colLength
            // 
            this.colLength.HeaderText = "长度";
            this.colLength.Name = "colLength";
            this.colLength.ReadOnly = true;
            this.colLength.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colDatas
            // 
            this.colDatas.HeaderText = "数据";
            this.colDatas.Name = "colDatas";
            this.colDatas.ReadOnly = true;
            this.colDatas.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.contextMenuStrip1.ShowImageMargin = false;
            this.contextMenuStrip1.Size = new System.Drawing.Size(36, 4);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssSocket,
            this.tssSerialPort,
            this.tssTransmitter,
            this.tssReply});
            this.statusStrip1.Location = new System.Drawing.Point(0, 490);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(751, 26);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tssSocket
            // 
            this.tssSocket.AutoSize = false;
            this.tssSocket.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tssSocket.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tssSocket.Name = "tssSocket";
            this.tssSocket.Size = new System.Drawing.Size(73, 21);
            this.tssSocket.Text = "tssRemote";
            this.tssSocket.Click += new System.EventHandler(this.tssSocket_Click);
            // 
            // tssSerialPort
            // 
            this.tssSerialPort.AutoSize = false;
            this.tssSerialPort.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tssSerialPort.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tssSerialPort.Name = "tssSerialPort";
            this.tssSerialPort.Size = new System.Drawing.Size(87, 21);
            this.tssSerialPort.Text = "tssSerialPort";
            this.tssSerialPort.Click += new System.EventHandler(this.tssSerialPort_Click);
            // 
            // tssTransmitter
            // 
            this.tssTransmitter.AutoSize = false;
            this.tssTransmitter.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tssTransmitter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tssTransmitter.Name = "tssTransmitter";
            this.tssTransmitter.Size = new System.Drawing.Size(87, 21);
            this.tssTransmitter.Text = "tssTransmitter";
            this.tssTransmitter.Click += new System.EventHandler(this.tssTransmitter_Click);
            // 
            // tssReply
            // 
            this.tssReply.AutoSize = false;
            this.tssReply.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tssReply.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tssReply.Name = "tssReply";
            this.tssReply.Size = new System.Drawing.Size(200, 21);
            this.tssReply.Text = "tssReply";
            this.tssReply.Click += new System.EventHandler(this.tssReply_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Black;
            this.imageList1.Images.SetKeyName(0, "t.ico");
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFile,
            this.mnuOp,
            this.mnuView,
            this.mnuSerialPort,
            this.mnuReply,
            this.mnuHelp});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip1.Size = new System.Drawing.Size(751, 25);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mnuFile
            // 
            this.mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuSaveLog,
            this.toolStripSeparator3,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Size = new System.Drawing.Size(58, 21);
            this.mnuFile.Text = "文件(&F)";
            // 
            // mnuSaveLog
            // 
            this.mnuSaveLog.Name = "mnuSaveLog";
            this.mnuSaveLog.Size = new System.Drawing.Size(124, 22);
            this.mnuSaveLog.Text = "保存(&S)...";
            this.mnuSaveLog.Click += new System.EventHandler(this.mnuSaveLog_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(121, 6);
            // 
            // mnuExit
            // 
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Size = new System.Drawing.Size(124, 22);
            this.mnuExit.Text = "退出(&X)";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuOp
            // 
            this.mnuOp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuCreateConnection,
            this.mnuLastConnection,
            this.mnuDisconnect,
            this.toolStripSeparator7,
            this.mnuSend,
            this.toolStripSeparator8,
            this.mnuClearLog});
            this.mnuOp.Name = "mnuOp";
            this.mnuOp.Size = new System.Drawing.Size(60, 21);
            this.mnuOp.Text = "操作(&C)";
            this.mnuOp.DropDownOpening += new System.EventHandler(this.mnuConnect_DropDownOpening);
            // 
            // mnuCreateConnection
            // 
            this.mnuCreateConnection.Name = "mnuCreateConnection";
            this.mnuCreateConnection.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.mnuCreateConnection.Size = new System.Drawing.Size(172, 22);
            this.mnuCreateConnection.Text = "新建连接(&N)...";
            this.mnuCreateConnection.Click += new System.EventHandler(this.mnuCreateConnection_Click);
            // 
            // mnuLastConnection
            // 
            this.mnuLastConnection.Name = "mnuLastConnection";
            this.mnuLastConnection.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.mnuLastConnection.Size = new System.Drawing.Size(172, 22);
            this.mnuLastConnection.Text = "最后连接(&L)";
            this.mnuLastConnection.Click += new System.EventHandler(this.mnuLastConnection_Click);
            // 
            // mnuDisconnect
            // 
            this.mnuDisconnect.Name = "mnuDisconnect";
            this.mnuDisconnect.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.mnuDisconnect.Size = new System.Drawing.Size(172, 22);
            this.mnuDisconnect.Text = "断开连接(&D)";
            this.mnuDisconnect.Click += new System.EventHandler(this.mnuDisconnect_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(169, 6);
            // 
            // mnuSend
            // 
            this.mnuSend.Name = "mnuSend";
            this.mnuSend.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.mnuSend.Size = new System.Drawing.Size(172, 22);
            this.mnuSend.Text = "发送";
            this.mnuSend.Click += new System.EventHandler(this.mnuSend_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(169, 6);
            // 
            // mnuClearLog
            // 
            this.mnuClearLog.Name = "mnuClearLog";
            this.mnuClearLog.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this.mnuClearLog.Size = new System.Drawing.Size(172, 22);
            this.mnuClearLog.Text = "清除记录(&C)";
            this.mnuClearLog.Click += new System.EventHandler(this.mnuClearLog_Click);
            // 
            // mnuView
            // 
            this.mnuView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuToolBar,
            this.mnuStatusBar,
            this.toolStripSeparator5,
            this.mnuAscii,
            this.mnuHex});
            this.mnuView.Name = "mnuView";
            this.mnuView.Size = new System.Drawing.Size(60, 21);
            this.mnuView.Text = "查看(&V)";
            this.mnuView.DropDownOpening += new System.EventHandler(this.mnuView_DropDownOpening);
            // 
            // mnuToolBar
            // 
            this.mnuToolBar.Name = "mnuToolBar";
            this.mnuToolBar.Size = new System.Drawing.Size(127, 22);
            this.mnuToolBar.Text = "工具栏(&T)";
            this.mnuToolBar.Click += new System.EventHandler(this.mnuToolBar_Click);
            // 
            // mnuStatusBar
            // 
            this.mnuStatusBar.Name = "mnuStatusBar";
            this.mnuStatusBar.Size = new System.Drawing.Size(127, 22);
            this.mnuStatusBar.Text = "状态栏(&S)";
            this.mnuStatusBar.Click += new System.EventHandler(this.mnuStatusBar_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(124, 6);
            // 
            // mnuAscii
            // 
            this.mnuAscii.Name = "mnuAscii";
            this.mnuAscii.Size = new System.Drawing.Size(127, 22);
            this.mnuAscii.Text = "Ascii(&A)";
            this.mnuAscii.Click += new System.EventHandler(this.mnuAscii_Click);
            // 
            // mnuHex
            // 
            this.mnuHex.Name = "mnuHex";
            this.mnuHex.Size = new System.Drawing.Size(127, 22);
            this.mnuHex.Text = "Hex(&H)";
            this.mnuHex.Click += new System.EventHandler(this.mnuHex_Click);
            // 
            // mnuSerialPort
            // 
            this.mnuSerialPort.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuOpenSerialPort,
            this.mnuCloseSerialPort,
            this.mnuEnableTransmit,
            this.toolStripSeparator1,
            this.mnuSerialPortSetting,
            this.mnuWrapperManager});
            this.mnuSerialPort.Name = "mnuSerialPort";
            this.mnuSerialPort.Size = new System.Drawing.Size(59, 21);
            this.mnuSerialPort.Text = "串口(&S)";
            // 
            // mnuOpenSerialPort
            // 
            this.mnuOpenSerialPort.Name = "mnuOpenSerialPort";
            this.mnuOpenSerialPort.Size = new System.Drawing.Size(129, 22);
            this.mnuOpenSerialPort.Text = "打开(&O)";
            this.mnuOpenSerialPort.Click += new System.EventHandler(this.mnuOpenSerialPort_Click);
            // 
            // mnuCloseSerialPort
            // 
            this.mnuCloseSerialPort.Name = "mnuCloseSerialPort";
            this.mnuCloseSerialPort.Size = new System.Drawing.Size(129, 22);
            this.mnuCloseSerialPort.Text = "关闭(&C)";
            this.mnuCloseSerialPort.Click += new System.EventHandler(this.mnuCloseSerialPort_Click);
            // 
            // mnuEnableTransmit
            // 
            this.mnuEnableTransmit.Name = "mnuEnableTransmit";
            this.mnuEnableTransmit.Size = new System.Drawing.Size(129, 22);
            this.mnuEnableTransmit.Text = "转发(&E)";
            this.mnuEnableTransmit.Click += new System.EventHandler(this.mnuEnableTransmit_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(126, 6);
            // 
            // mnuSerialPortSetting
            // 
            this.mnuSerialPortSetting.Name = "mnuSerialPortSetting";
            this.mnuSerialPortSetting.Size = new System.Drawing.Size(129, 22);
            this.mnuSerialPortSetting.Text = "设置(&S)...";
            this.mnuSerialPortSetting.Click += new System.EventHandler(this.mnuSerialPortSetting_Click);
            // 
            // mnuWrapperManager
            // 
            this.mnuWrapperManager.Name = "mnuWrapperManager";
            this.mnuWrapperManager.Size = new System.Drawing.Size(129, 22);
            this.mnuWrapperManager.Text = "包装(&W)...";
            this.mnuWrapperManager.Click += new System.EventHandler(this.mnuWrapperManager_Click);
            // 
            // mnuReply
            // 
            this.mnuReply.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuEnableReply,
            this.mnuReplySetting,
            this.toolStripSeparator2,
            this.mnuEnabledReplyModbus,
            this.mnuReplyMbSettings});
            this.mnuReply.Name = "mnuReply";
            this.mnuReply.Size = new System.Drawing.Size(60, 21);
            this.mnuReply.Text = "回复(&R)";
            // 
            // mnuEnableReply
            // 
            this.mnuEnableReply.Name = "mnuEnableReply";
            this.mnuEnableReply.Size = new System.Drawing.Size(170, 22);
            this.mnuEnableReply.Text = "启用回复(&A)";
            this.mnuEnableReply.Click += new System.EventHandler(this.mnuEnableReply_Click);
            // 
            // mnuEnabledReplyModbus
            // 
            this.mnuEnabledReplyModbus.Name = "mnuEnabledReplyModbus";
            this.mnuEnabledReplyModbus.Size = new System.Drawing.Size(170, 22);
            this.mnuEnabledReplyModbus.Text = "启用回复MB(&B)";
            this.mnuEnabledReplyModbus.Click += new System.EventHandler(this.mnuEnabledReplyModbus_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(167, 6);
            // 
            // mnuReplySetting
            // 
            this.mnuReplySetting.Name = "mnuReplySetting";
            this.mnuReplySetting.Size = new System.Drawing.Size(170, 22);
            this.mnuReplySetting.Text = "设置(&S)...";
            this.mnuReplySetting.Click += new System.EventHandler(this.mnuReplySetting_Click);
            // 
            // mnuHelp
            // 
            this.mnuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAbout});
            this.mnuHelp.Name = "mnuHelp";
            this.mnuHelp.Size = new System.Drawing.Size(61, 21);
            this.mnuHelp.Text = "帮助(&H)";
            // 
            // mnuAbout
            // 
            this.mnuAbout.Name = "mnuAbout";
            this.mnuAbout.Size = new System.Drawing.Size(125, 22);
            this.mnuAbout.Text = "关于(&A)...";
            this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
            // 
            // dgvContextMenu
            // 
            this.dgvContextMenu.Name = "dgvContextMenu";
            this.dgvContextMenu.Size = new System.Drawing.Size(61, 4);
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbCreateConnection,
            this.tsbLastConnection,
            this.tsbDisconnection,
            this.toolStripSeparator4,
            this.tsbSave,
            this.tsbHex,
            this.tsbAscii,
            this.tsbClear,
            this.tsbReply});
            this.toolStrip1.Location = new System.Drawing.Point(0, 25);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(751, 25);
            this.toolStrip1.TabIndex = 14;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbCreateConnection
            // 
            this.tsbCreateConnection.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbCreateConnection.Image = global::SocketClient.Properties.Resources.connection;
            this.tsbCreateConnection.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCreateConnection.Name = "tsbCreateConnection";
            this.tsbCreateConnection.Size = new System.Drawing.Size(23, 22);
            this.tsbCreateConnection.Text = "新建连接";
            this.tsbCreateConnection.Click += new System.EventHandler(this.tsbCreateConnection_Click);
            // 
            // tsbLastConnection
            // 
            this.tsbLastConnection.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbLastConnection.Image = global::SocketClient.Properties.Resources.oldConnection;
            this.tsbLastConnection.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLastConnection.Name = "tsbLastConnection";
            this.tsbLastConnection.Size = new System.Drawing.Size(23, 22);
            this.tsbLastConnection.Text = "最后连接";
            this.tsbLastConnection.Click += new System.EventHandler(this.tsbLastConnection_Click);
            // 
            // tsbDisconnection
            // 
            this.tsbDisconnection.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbDisconnection.Image = global::SocketClient.Properties.Resources.disconnection;
            this.tsbDisconnection.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDisconnection.Name = "tsbDisconnection";
            this.tsbDisconnection.Size = new System.Drawing.Size(23, 22);
            this.tsbDisconnection.Text = "断开连接(&D)";
            this.tsbDisconnection.Click += new System.EventHandler(this.tsbDisconnection_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbSave
            // 
            this.tsbSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbSave.Image = global::SocketClient.Properties.Resources.save;
            this.tsbSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSave.Name = "tsbSave";
            this.tsbSave.Size = new System.Drawing.Size(23, 22);
            this.tsbSave.Text = "保存(&S)";
            this.tsbSave.Click += new System.EventHandler(this.tsbSave_Click);
            // 
            // tsbHex
            // 
            this.tsbHex.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbHex.Image = global::SocketClient.Properties.Resources.hex;
            this.tsbHex.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbHex.Name = "tsbHex";
            this.tsbHex.Size = new System.Drawing.Size(23, 22);
            this.tsbHex.Text = "Hex(&H)";
            this.tsbHex.Click += new System.EventHandler(this.tsbHex_Click);
            // 
            // tsbAscii
            // 
            this.tsbAscii.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAscii.Image = global::SocketClient.Properties.Resources.ab;
            this.tsbAscii.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAscii.Name = "tsbAscii";
            this.tsbAscii.Size = new System.Drawing.Size(23, 22);
            this.tsbAscii.Text = "Ascii(&S)";
            this.tsbAscii.Click += new System.EventHandler(this.tsbAscii_Click);
            // 
            // tsbClear
            // 
            this.tsbClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbClear.Image = global::SocketClient.Properties.Resources.clear;
            this.tsbClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbClear.Name = "tsbClear";
            this.tsbClear.Size = new System.Drawing.Size(23, 22);
            this.tsbClear.Text = "清除(&L)";
            this.tsbClear.Click += new System.EventHandler(this.tsbClear_Click);
            // 
            // tsbReply
            // 
            this.tsbReply.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbReply.Image = global::SocketClient.Properties.Resources.reply;
            this.tsbReply.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbReply.Name = "tsbReply";
            this.tsbReply.Size = new System.Drawing.Size(23, 22);
            this.tsbReply.Text = "toolStripButton1";
            this.tsbReply.Click += new System.EventHandler(this.tsbReply_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 50);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dataGridView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.hexSend);
            this.splitContainer1.Panel2.Controls.Add(this.tsSend);
            this.splitContainer1.Panel2MinSize = 200;
            this.splitContainer1.Size = new System.Drawing.Size(751, 440);
            this.splitContainer1.SplitterDistance = 172;
            this.splitContainer1.SplitterWidth = 8;
            this.splitContainer1.TabIndex = 15;
            // 
            // hexSend
            // 
            // 
            // 
            // 
            this.hexSend.BuiltInContextMenu.CopyHexMenuItemImage = global::SocketClient.Properties.Resources.CopyHS;
            this.hexSend.BuiltInContextMenu.CopyHexMenuItemText = "复制十六进制(&D)";
            this.hexSend.BuiltInContextMenu.CopyMenuItemImage = global::SocketClient.Properties.Resources.CopyHS;
            this.hexSend.BuiltInContextMenu.CopyMenuItemText = "复制(&C)";
            this.hexSend.BuiltInContextMenu.CutMenuItemImage = global::SocketClient.Properties.Resources.CutHS;
            this.hexSend.BuiltInContextMenu.CutMenuItemText = "剪切(&T)";
            this.hexSend.BuiltInContextMenu.PasteHexMenuItemImage = global::SocketClient.Properties.Resources.PasteHS;
            this.hexSend.BuiltInContextMenu.PasteHexMenuItemText = "粘贴十六进制(&O)";
            this.hexSend.BuiltInContextMenu.PasteMenuItemImage = global::SocketClient.Properties.Resources.PasteHS;
            this.hexSend.BuiltInContextMenu.PasteMenuItemText = "粘贴(&P)";
            this.hexSend.BuiltInContextMenu.SelectAllMenuItemText = "全选(&A)";
            this.hexSend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hexSend.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.hexSend.Location = new System.Drawing.Point(0, 25);
            this.hexSend.Name = "hexSend";
            this.hexSend.ShadowSelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(60)))), ((int)(((byte)(188)))), ((int)(((byte)(255)))));
            this.hexSend.Size = new System.Drawing.Size(751, 235);
            this.hexSend.TabIndex = 12;
            // 
            // tsSend
            // 
            this.tsSend.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsSend.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbShowRow,
            this.tsbShowCol,
            this.tsbSp1,
            this.tsbClearSend,
            this.tsbSendHistory,
            this.tsbSend});
            this.tsSend.Location = new System.Drawing.Point(0, 0);
            this.tsSend.Name = "tsSend";
            this.tsSend.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tsSend.Size = new System.Drawing.Size(751, 25);
            this.tsSend.TabIndex = 15;
            this.tsSend.Text = "toolStrip2";
            // 
            // tsbShowRow
            // 
            this.tsbShowRow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbShowRow.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowRow.Image")));
            this.tsbShowRow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowRow.Name = "tsbShowRow";
            this.tsbShowRow.Size = new System.Drawing.Size(36, 22);
            this.tsbShowRow.Text = "行号";
            this.tsbShowRow.ToolTipText = "显示行号";
            this.tsbShowRow.Click += new System.EventHandler(this.tsbShowRow_Click);
            // 
            // tsbShowCol
            // 
            this.tsbShowCol.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbShowCol.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowCol.Image")));
            this.tsbShowCol.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowCol.Name = "tsbShowCol";
            this.tsbShowCol.Size = new System.Drawing.Size(36, 22);
            this.tsbShowCol.Text = "列号";
            this.tsbShowCol.Click += new System.EventHandler(this.tsbShowCol_Click);
            // 
            // tsbSp1
            // 
            this.tsbSp1.Name = "tsbSp1";
            this.tsbSp1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbClearSend
            // 
            this.tsbClearSend.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbClearSend.Image = ((System.Drawing.Image)(resources.GetObject("tsbClearSend.Image")));
            this.tsbClearSend.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbClearSend.Name = "tsbClearSend";
            this.tsbClearSend.Size = new System.Drawing.Size(50, 22);
            this.tsbClearSend.Text = "清除(&L)";
            this.tsbClearSend.Click += new System.EventHandler(this.tsbClearSend_Click);
            // 
            // tsbSendHistory
            // 
            this.tsbSendHistory.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbSendHistory.Image = ((System.Drawing.Image)(resources.GetObject("tsbSendHistory.Image")));
            this.tsbSendHistory.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSendHistory.Name = "tsbSendHistory";
            this.tsbSendHistory.Size = new System.Drawing.Size(53, 22);
            this.tsbSendHistory.Text = "历史(&H)";
            this.tsbSendHistory.Click += new System.EventHandler(this.tsbSendHistory_Click);
            // 
            // tsbSend
            // 
            this.tsbSend.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbSend.Image = ((System.Drawing.Image)(resources.GetObject("tsbSend.Image")));
            this.tsbSend.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSend.Name = "tsbSend";
            this.tsbSend.Size = new System.Drawing.Size(57, 22);
            this.tsbSend.Text = "发送(F5)";
            this.tsbSend.Click += new System.EventHandler(this.tsbSend_Click);
            // 
            // mnuReplyMbSettings
            // 
            this.mnuReplyMbSettings.Name = "mnuReplyMbSettings";
            this.mnuReplyMbSettings.Size = new System.Drawing.Size(170, 22);
            this.mnuReplyMbSettings.Text = "设置回复MB(&D)...";
            this.mnuReplyMbSettings.Click += new System.EventHandler(this.mnuReplyMbSettings_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 516);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Socket Debugger";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tsSend.ResumeLayout(false);
            this.tsSend.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tssSocket;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolStripStatusLabel tssSerialPort;
        private System.Windows.Forms.ToolStripStatusLabel tssTransmitter;
        private System.Windows.Forms.ToolStripStatusLabel tssReply;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuFile;
        private System.Windows.Forms.ToolStripMenuItem mnuExit;
        private System.Windows.Forms.ToolStripMenuItem mnuHelp;
        private System.Windows.Forms.ToolStripMenuItem mnuAbout;
        private System.Windows.Forms.ToolStripMenuItem mnuSerialPort;
        private System.Windows.Forms.ToolStripMenuItem mnuOpenSerialPort;
        private System.Windows.Forms.ToolStripMenuItem mnuCloseSerialPort;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mnuSerialPortSetting;
        private System.Windows.Forms.ToolStripMenuItem mnuReply;
        private System.Windows.Forms.ToolStripMenuItem mnuEnableReply;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem mnuReplySetting;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStripMenuItem mnuEnableTransmit;
        private System.Windows.Forms.ToolStripMenuItem mnuSaveLog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem mnuWrapperManager;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDT;
        private System.Windows.Forms.DataGridViewTextBoxColumn colS;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSCDir;
        private System.Windows.Forms.DataGridViewTextBoxColumn colC;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCComDir;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCom;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLength;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDatas;
        private System.Windows.Forms.ContextMenuStrip dgvContextMenu;
        private System.Windows.Forms.ToolStripMenuItem mnuOp;
        private System.Windows.Forms.ToolStripMenuItem mnuCreateConnection;
        private System.Windows.Forms.ToolStripMenuItem mnuDisconnect;
        private System.Windows.Forms.ToolStripMenuItem mnuView;
        private System.Windows.Forms.ToolStripMenuItem mnuAscii;
        private System.Windows.Forms.ToolStripMenuItem mnuHex;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbHex;
        private System.Windows.Forms.ToolStripButton tsbAscii;
        private System.Windows.Forms.ToolStripButton tsbClear;
        private System.Windows.Forms.ToolStripButton tsbSave;
        private System.Windows.Forms.ToolStripButton tsbCreateConnection;
        private System.Windows.Forms.ToolStripButton tsbLastConnection;
        private System.Windows.Forms.ToolStripButton tsbDisconnection;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem mnuLastConnection;
        private System.Windows.Forms.ToolStripMenuItem mnuToolBar;
        private System.Windows.Forms.ToolStripMenuItem mnuStatusBar;
        private Be.Windows.Forms.HexBox hexSend;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem mnuClearLog;
        private System.Windows.Forms.ToolStripMenuItem mnuSend;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStrip tsSend;
        private System.Windows.Forms.ToolStripButton tsbShowRow;
        private System.Windows.Forms.ToolStripButton tsbShowCol;
        private System.Windows.Forms.ToolStripSeparator tsbSp1;
        private System.Windows.Forms.ToolStripButton tsbSendHistory;
        private System.Windows.Forms.ToolStripButton tsbSend;
        private System.Windows.Forms.ToolStripButton tsbClearSend;
        private System.Windows.Forms.ToolStripButton tsbReply;
        private System.Windows.Forms.ToolStripMenuItem mnuEnabledReplyModbus;
        private System.Windows.Forms.ToolStripMenuItem mnuReplyMbSettings;
    }
}

