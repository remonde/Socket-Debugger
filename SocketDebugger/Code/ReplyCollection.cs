﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SocketClient {
    /// <summary>
    /// 
    /// </summary>
    public class ReplyList : System.Collections.Generic.List<ReplyItem> {

        public ModbusReplyItem GetModbusReplyItem() {
            var mri = (ModbusReplyItem)this.First(mriLba => mriLba.GetType() == typeof(ModbusReplyItem));
            return mri;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bs"></param>
        /// <returns></returns>
        public byte[] GetSendBytes(byte[] bs) {
            foreach (ReplyItem item in this) {
                if (item is ModbusReplyItem) {
                    if (this.EnabledModbus) {
                        if (item.Match(bs)) {
                            return item.GetReplyBytes();
                        }
                    }
                } else {
                    if (item.Match(bs)) {
                        return item.GetReplyBytes();
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Enabled {
            get;
            set;
        }


        /// <summary>
        /// 
        /// </summary>
        public bool EnabledModbus { get; set; }


        internal void Remove(ReplyGroup rg) {
            for (int i = this.Count - 1; i >= 0; i--) {
                if (this[i].ReplyGroup == rg) {
                    this.RemoveAt(i);
                }
            }
        }
    }
}
