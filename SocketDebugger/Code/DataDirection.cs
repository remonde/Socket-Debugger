﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

namespace SocketClient
{
    public class DataDirection
    {
        static public readonly DataDirection In = new DataDirection(        
            Color.FromArgb(0xff, 0xd3, 0xe5, 0xfa)
            );
        static public DataDirection Out = new DataDirection(
            Color.FromArgb(0XFF, 0xF6, 0xE7, 0xB0)            
            );

        static public DataDirection Unknown = new DataDirection(
            Color.White);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="backgroundColor"></param>
        private DataDirection(Color backgroundColor)
        {
            this.BackColor = backgroundColor;
        }

        #region BackgroundColor
        /// <summary>
        /// 
        /// </summary>
        public Color BackColor
        {
            get
            {
                return _backColor;
            }
            set
            {
                _backColor = value;
            }
        } private Color _backColor;
        #endregion //BackgroundColor
    }
}
