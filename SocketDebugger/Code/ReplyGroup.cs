using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;

namespace SocketClient {

    public class ReplyGroup : IComparable<ReplyGroup> {
        public ReplyGroup(string name) {
            this.Name = name;
        }
#region Name
        /// <summary>
        /// 
        /// </summary>
        public string Name {
            get {
                if (_name == null) {
                    _name = string.Empty;
                }
                return _name;
            }
            set {
                _name = value;
            }
        } private string _name;
#endregion //Name


        public int CompareTo(ReplyGroup other) {
            return this.Name.CompareTo(other.Name);
        }
    }
}
