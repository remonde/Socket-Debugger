using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;

namespace SocketClient {

    public class ReplyGroupCollection : List<ReplyGroup> {
        public ReplyGroup GetFirstOrDefault() {
            if (this.Count > 0) {
                return this[0];
            } else {
                this.Add(new ReplyGroup("default"));
                return this[0];
            }

        }
    }
}
