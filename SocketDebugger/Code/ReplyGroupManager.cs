using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;

namespace SocketClient {

    public class ReplyGroupManager {
        public static ReplyGroupManager Instance {
            get {
                return _instance;
            }
        }private static ReplyGroupManager _instance = new ReplyGroupManager();

        private ReplyGroupManager() {

        }
        public ReplyGroup this[string name] {
            get {
                foreach (var item in this.ReplyGroups) {
                    if (string.Compare(item.Name.Trim(), name.Trim(), true) == 0) {
                        return item;
                    }
                }

                ReplyGroup newItem = new ReplyGroup(name);
                this.ReplyGroups.Add(newItem);
                return newItem;
            }
        }

#region ReplyGroups
        /// <summary>
        /// 
        /// </summary>
        public ReplyGroupCollection ReplyGroups {
            get {
                if (_replyGroups == null) {
                    _replyGroups = new ReplyGroupCollection();
                }
                return _replyGroups;
            }
            set {
                _replyGroups = value;
            }
        } private ReplyGroupCollection _replyGroups;
#endregion //ReplyGroups

    }
}
